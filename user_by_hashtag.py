import hashtag_internal


def main():
    app = hashtag_internal.Hashtag()
    app.render()
    pass


if __name__ == '__main__':
    main()
